# Ask Noah Show Giveaway Script

This script was used to get the users that won the Ask Noah Show Telegram Giveaway, which was a giveaway ran by the Ask Noah Show that awarded the following prizes once the group reached 512 members:
 - A $51.20 Amazon gift card to the 512th member.
 - Everyone in the group is entered to win a $128 gift card.

You can find the Telegram group we used this with at [@asknoahshow](https://t.me/asknoahshow).

While this simple script could be kept private, the Ask Noah Show believes in the value of Open Source, so we decided to share it.

This script simply needs the Telethon Python 3 library. You can install this by running `pip3 install telethon`.

# About the Ask Noah Show

The Ask Noah Show is a weekly radio call-in show where we take your tech questions or business in tech questions live on the air. The show airs Tuesdays at 6 PM US Central Time online at [asknoahshow.com](http://www.asknoahshow.com/) and on the radio at KEQQ 88.3 LPFM in Grand Forks, North Dakota.
